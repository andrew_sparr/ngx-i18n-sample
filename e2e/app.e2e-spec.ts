import { NgxI18nSamplePage } from './app.po';

describe('ngx-i18n-sample App', () => {
  let page: NgxI18nSamplePage;

  beforeEach(() => {
    page = new NgxI18nSamplePage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
