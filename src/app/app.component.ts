import { Component } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  // param = {value: 'world'};

  constructor(private translate: TranslateService) {
      // // this language will be used as a fallback when a translation isn't found in the current language
      // translate.setDefaultLang('en');

      // // the lang to use, if the lang isn't available, it will use the current loader to get them
      // translate.use('de');

      translate.addLangs(["en", "de"]);
      translate.setDefaultLang('de');
      debugger;

      let browserLang = translate.getBrowserLang();
      translate.use(browserLang.match(/en|de/) ? browserLang : 'en');
  }
}
